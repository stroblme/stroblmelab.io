---
title: This is ME
layout: page
comments: false
thumbnail: /images/about/about_h.jpg
---

## MElvin's PROjects

I'm studying Electro- and Informationscience at the KIT in Karlsruhe and love combining various skills to craft exceptional projects in my free-time.

From time to time I take a dive into photography. You can view the results on my 500px site by clicking on the icon in the navigation bar.

All the projects listed here are also available on gitlab, where you can get again, by clicking on the icon.
I would be happy if the knowledge provided there, would help anyone for his/ her own project. Don't be scared by the namings of the projects. I have a faible for awkward namings and abbreviations. ;-)

You can subscribe on this site [using the RSS/ Atom-feed](https://stroblme.gitlab.io/atom.xml "RSS-Feed"). I will try to add some already finished project from the past and will keep on writing about projects I'm currently on.

If you have any questions, ideas, or want to share opinions, you can contact me by... I guess you know how. Join me for a coffee when you're around ;)

***

#### Get Inspired and Take What You Need!

***

If you want to use those ideas, plans or sources for commercial purposes, please contact me.

***

#### Page Status:

Just an information if the page is currently updating or whether all pipelines are finished.

[![pipeline status](https://gitlab.com/stroblme/stroblme.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/stroblme/stroblme.gitlab.io/commits/master)
