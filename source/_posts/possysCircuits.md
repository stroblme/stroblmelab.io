---
title: POSSYS - Circuits
comments: false
thumbnail: /images/possys/powerRouter/3d.png
date: 2019-05-02 09:06:31
tags:
- audio
- newbuild
- crafted
categories:
- possys
---

## Routing Power Supply and Signals in POSSYS

***
![3D View](/images/possys/powerRouter/3d.png)

![Circuit](/images/possys/powerRouter/circuit.png)

![Layout](/images/possys/powerRouter/layout.png)
***