---
title: Tiny House Aufbau - 06-24
comments: false
thumbnail: /images/tinyhouse/tinyHouseTimelapse.jpg
date: 2019-06-24 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - 06-24
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-06-24_timelapse.webm" type="video/webm">
    </video>
</div>
