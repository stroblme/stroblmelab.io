---
title: The Life Without
comments: false
thumbnail: /images/theLifeWithout/blurredCityPeople.jpg
date: 2019-07-10 12:01:09
tags:
- stories
- blog
categories:
- general
---

# The Life Without

### Introduction

Since I owned my first Android Device, which was at the age of 16, I always wanted to customize those tiny gadgets to the limit. I never liked the way big companies enforce their System design and functionalities on the user. Therefore, I always searched for possibilities to underway those restrictions and "suggestions".

But don't panic, I won't get too technical in this article, I just wanted to provide some background knowledge about me. In the series "The Life Without" I want to give an idea why I quit with Google, Facebook, Whatsapp and other big commercial companies.

Futhermore, this article should not be seen as a solution for getting "offline". I like the technology we have and, as an engineer, also want to get the most out of it, but "with great power comes great responsibility".
In this particular case: Not for the financial. Not for the environment. Not for the society. But for our data.

### Data

Data is something what humans don't have a sense for.
We can see an apple and can imagine that a tree of apples could be a lot of them.
We can taste spices and have an idea of how much of it is needed to make a delicious meal.
We can hear a bird and know that many of them could make a respectable noise.

Real things are scaling. Data don't.
Real things can be crafted. Data exists.
Real things can be destroyed. Hardly.

Getting a feeling for data might be nearly impossible, but understanding the mechanism which processes it, isn't.

Friends often asked why I take all the effort, create all those circumstances and spend so much time just to stay a little bit under the radar. It's then pretty hard to find the right words, which can describe it in a "chatty" way without getting too technical.

In my opinion, knowing what happens with our data and the reason why big tech companies collect so many of them shouldn't be a privilege for geeky people.

### My History of Quitting

In school times I mostly used Whatsapp, Facebook and Google. From time to time I also used Snapchat to stay in contact with some friends in America.
During that time, I wouldn't describe myself as someone who extensively used those services. They where just some helpfull tools to organize events, keep contact with friends and even a bit profiling.

When I begun studying, I learned new friends and all the technical knowledge and experiences I teached myself so far, which was something one wouldn't talk about in school, gained me some respect there.

As Facebook lost it's importance for our generation, I decided to delete my account there as there was no need for it anymore.

In the meantime I gained some interest on privacy and independence in the context of technical information technology.

I therefore begun to replace apps from big tech firms with open source alternatives on my smartphone and laptop.

As Android restricted to delete system apps (which include most apps from google) I found that the Cyanogen Mod Project seemed as a good solution to achieve my goals.

It was January 2019 when "Disa", my application which I used up to that time for managing Whatsapp, Telegram, Facebook and SMS through one single app, notified me, that Whatsapp will prevent thrid party apps from accessing their services.

As a result, my whatsapp stopped working on the 1st of February.
The only option left, was to download the original Whatsapp application.

By that time, I grew myself a strong interest in information privacy and independence, so I wasn't happy about installing Whatsapp (which belongs to Facebook since 2017) and share additional device information with this company.

One might think where's the difference between using Whatsapp and installing the corresponding application?

Well, in the first case, Whatsapp only sees some in- and outcoming messages. They can see who and when I texted someone and they see the message size.
In the second case, they additionally see the type of device I'm using, have direct access to Camera, FileSystem, Location, Accounts and lot lot more.

As you might see, "Disa" provided some layer of anonymity/ security between the user and the company. Later, I will describe why this is so important to me.

I wouldn't write this article if a consequence of those facts wouldn't have been the deletion of my Whatsapp account.
It was quit an exciting moment, after confirming several times that I'm really sure to permanently delete all my account information.
Unfortunately, I wasn't able to notify my friends of this action, as Whatsapp already disabled it's service for third party apps. Still sorry about that!!

### 3 Month later

It's now end of May while I'm writing those lines, sitting in a Camper somewhere in the eastern part of Karlsruhe.

In the first few weeks, I was busy, notifying all my friends that they won't reach me on Whatsapp anymore. I spread my EMail address and tried to encourage some to install Telegram or Signal, which both I was using with some of my friends up to that time.

I was able to convince 12 friends of mine of joining Telegram/ Signal, which, together with those friends who already had those messengers, was a quite decent number.

With the rest of my friends I keep contact using EMail and SMS, which both works surprisingly well.

So, how ist the life without?
Well I wouldn't want to go back! I can contact everyone like before and comfortably write with the closer friends using Telegram/ Signal.

Deleting Whatsapp killed immediately those tiny, meaningless, smalltalk conversations one might start just by being boring.
I started phoning friends more often, which turned out being a far more personal and also efficient way instead of texting.

### Reactions

My best friends heavily uses Instagram, Snapchat, Facebook and other communication services I'm currently might not be aware of. Of course he uses Whatsapp.
He asked me if it wouldn't be annoying, being on an event and asking a girl for her mail address or explaining why she wouldn't be able to contact me via Whatsapp.

I found that explaining the own statement is everything but annoying. When I experience cases where I excused my behavior by lies rather than my real opinion, then I'm actually questioning my opinion. THAT is annoying for both sides.
If one is really convinced of the own opinion then you actually can make discussions.
So in fact, explaining all this is rather the start of a good discussion than an apology. It the other side wasn't of that opinion then it wasn't it worth at all ;)

Some even asked if I would feel like being socially sidelined as I'm not part of chat groups nor can share images on an online profile.

While being not in Whatsapp groups is really a drawback from time to time, profiling myself on online platform definitely isn't.
Regarding the groups, I made the experience, that there's always at least one person which has Telegram or Signal or is willing to keep me informed on other ways.
And as most of the time, groups are used to send shitty images or clips around I doesn't miss that much.

It might sound ridiculous when I write that I don't what to profile myself on an online blog. More on this in the next chapter.

### Controversy

##### Blogging

Catching up the last statement from the previous chapter, I found that it's actually a great difference between writing on an open-source blog where I can control the exact amount of information I share and posting pics or sharing my opinion in 280 chars while providing commercial companies as much information as they wan't for advertising and general data collection.

##### Other Companies

While I successfully quitted with Google, Facebook and other social platforms, I'm still using e.g. Amazon and (of-course) their webservices (AWS).
In fact this is really something I'm working on, but where I wasn't able to find suitable alternatives.
Sure; most of the products offered on Amazon can also be bought on other websites, but in most cases I would have to pay more money.
I will create another post discussing the issues of surfing and purchasing without amazon.

##### Relying on friends

As I previously stated, I would miss some information from groups and events if my friends wouldn't inform me on that.
This issue basically addresses the fact, that the majority of the people do not have Signal or Telegram nor spend effort in keeping data private.
This amount of people can be splitted up in four groups:

- The ones who value private data, but are too lazy to spend effort in keeping their data privat
- The ones who value private data, wan't to spend effort, but have no idea how
- The ones who value private data only when 7/8 of the moon is visible
- The ones giving a shit about their private data

In reference to my friends, I would give each group an equal amount of people.
I wouldn't write all this if I wouldn't be convinced that this distribution would shift in the next few years (hopefully in the right direction)

Finding motivation for spending effort on keeping data privat is quite a hard thing.
Being consequent on this is even harder.

As I currently can't provide an All-Winner-Plan which helps to keep data private without making any cuts, I hope I could help at least with the motivational part ;)
