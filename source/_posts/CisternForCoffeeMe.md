---
title: Cistern For CoffeeMe
comments: false
thumbnail: /images/coffeeme/watertank_iso.jpg
date: 2018-11-09 19:59:03
tags:
- crafted
categories:
- coffeeme
---

## Stainless-Steel Water Tank

Being on the search for a stylish cistern, fitting in the CoffeeMe project, a workmate gave me a former compressed-air tank.
Besides this item is matching the robust design very well and accomplished the limited space I have in this project while it also had the perfect size of nearly 3 cups of coffee.

![Cistern Iso](/images/coffeeme/watertank_iso.jpg)
![Cistern Front](/images/coffeeme/watertank_front.jpg)

The two sides of the cylinder have an 1/4" mount, which I can use for connecting a compressed air quick release fastener (It's not much better in german).
This also solves my problem of how I ensure that no water is dripping out, while removing the cistern for refill.