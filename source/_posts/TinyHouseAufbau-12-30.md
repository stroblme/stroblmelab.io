---
title: Tiny House Aufbau - 12-30
comments: false
thumbnail: /images/tinyhouse/tinyHouseTimelapse.jpg
date: 2019-12-30 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - 12-30
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-12-30_timelapse.webm" type="video/webm">
    </video>
</div>
