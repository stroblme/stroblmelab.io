---
title: HAUCO - The History
comments: false
thumbnail: /images/empty.jpg
date: 2019-03-01 12:04:59
tags:
- stories
categories:
- hauco
- automation
---

## Previous Version of my Home Automation System

In Winter 2015/16 I crafted my first Home Automation System. I then used it for nearly three years in my student housing for controlling light and remote power relays.
The System consists of a Raspberry PI on which runs "OpenHab" as server.

The second Part is a custom build ATMega128 based board which receives control signals from the RPI3 via I2C.
It can drive 12V Leds on three channels with up to 5A each. Furthermore it contains a modified 433MHz circuit which can be used to control remote receivers. Those sequences are hard-coded in the mikrocontroller, but can transmit on 256 different channels.
I also designed a small 8-channel relais driver circuit but never used it.

Besides it functionality as 24/7 home automation server, the RPI3 acted also as headless download server.

Most of the abstract automation stuff came directly from the "Tasker" application running on my phone. As all outputs can be controlled by making simple http requests, this proved as a pretty comfortable solution.
In the end, my phone turned off the light when I went to sleep and turned it back on when the phone alarm rings.

I also created some shortcuts on my phone so that I was able to easily control my remote relays.