---
title: CoffeeMe - Inital Test
comments: false
thumbnail: /images/coffeeme/initialTest.jpg
date: 2019-07-03 22:14:21
tags:
- design
- rebuild
categories:
- coffeeme
---

A test Video from the first working version. Currently, there is neither a coffee bean -, nor a waste case.

All functionalities are working, but the manometer needs some improvements, as it reacts too fast to pressure changes. This results in a shaky pointer.

***

<div class="row post-image-bg">
    <video width="99%" height="540" controls muted>
        <source src="/images/coffeeme/initialTest.mp4" type="video/mp4">
    </video>
</div>
