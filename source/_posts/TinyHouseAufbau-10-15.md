---
title: Tiny House Aufbau - Dämmung V2 und Schiebetür
comments: false
thumbnail: /images/tinyhouse/GlaselementEingebaut.jpg
date: 2019-10-15 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - Dämmung V2 und Schiebetür
## A Timelapse

Zunächst musste das ganze Häuschen etwas nach vorne bewegt werden

![Vorziehen](/images/tinyhouse/Vorziehen.jpg)

Dann wurde der erste Glasflügel hineingetragen

![Flügel](/images/tinyhouse/Flügel.jpg)

Und anschließend der ganze Rest quer hineinbuchsiert.

![GlaselementEingebaut](/images/tinyhouse/GlaselementEingebaut.jpg)

Noch ein wenig Feinschliff, damit alle Kanten sauber passen:

![Feinschliff](/images/tinyhouse/Feinschliff.jpg)


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-10-15_timelapse.webm" type="video/webm">
    </video>
</div>

Und ein kurzes Video von dem Aufstieg auf 4m Höhe:

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/wayToTheTop.webm" type="video/webm">
    </video>
</div>
