---
title: Tiny House Aufbau - Erste Fahrt
comments: false
thumbnail: /images/tinyhouse/gespann.jpg
date: 2019-10-15 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - Erste Fahrt

Zum allerersten mal, seit dem Materialtranport wird der Anhänger wieder bewegt.
Diesmal mit dem gesamten Aufbau sowie Verglasung.

## Erstmal ausparken das "Kleine"

Die erste Hürde: Sicher aus dem Unterstand rausfahren.
Kein Schongang, kein zweiter Versuch. Und im Rückspiegel: Eine rote Wand.

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/anhaengen.webm" type="video/webm">
    </video>
</div>

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/ausparken.webm" type="video/webm">
    </video>
</div>

## Ab auf die Waage damit

Nun folgt eine etwas interessante Fahrt ein paar Orte weiter zu einer Waage, mit dem die Masse des Frischlings bestimmt werden soll.

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/wiegen.webm" type="video/webm">
    </video>
</div>

**3120 kg** ist das Kleine schwer.
Damit liegt das gemessene Gewicht etwa 100 kg über dem berechneten.
Das hätte auch schlimmer ausgehen können. Ich denke fürs erste ist das ein ganz gutes Ergebnis.

## Wieder zurück..

.. diesmal ein paar Meter weiter hoch, auf einen Abstellplatz im Freien.

<!-- <div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/rueckfahrt.webm" type="video/webm">
    </video>
</div> -->
**Coming soon**


## Plane drüber!

Da die Fensterbänke noch fehlen und das Dach noch nicht vollständig abgedichtet ist, kommt vorerst nochmal eine Plane drüber.
Im Alleingang und bei böhigem Wind stellte sich das ganze dann als mittelgroße Herausforderung dar.

**Coming soon**
<!-- <div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/abdecken.webm" type="video/webm">
    </video>
</div> -->
