---
title: Tiny House Aufbau - 11-10
comments: false
thumbnail: /images/tinyhouse/3AufDerLeiter.jpg
date: 2019-11-10 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - 11-10
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-11-10_timelapse.webm" type="video/webm">
    </video>
</div>

![Dach](/images/tinyhouse/dach.jpg)
