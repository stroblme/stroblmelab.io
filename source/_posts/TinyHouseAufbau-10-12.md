---
title: Tiny House Aufbau - Grundierung und Rollen Drehen
comments: false
thumbnail: /images/tinyhouse/tinyHouseTimelapse.jpg
date: 2019-10-12 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - Grundierung und Rollen Drehen
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-10-12_timelapse.webm" type="video/webm">
    </video>
</div>

For sure, things not always go as expected. So the paint roller I ordered, didn't fit on the rollers we had at home, as the diameter was too big.

As we planned to paint the Tiny House on the next day, I spend some time on the turning machine, crafting an adapter:

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/rollenDrehen.webm" type="video/webm">
    </video>
</div>
