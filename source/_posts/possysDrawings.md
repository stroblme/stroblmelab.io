---
title: POSSYS - Drawings
comments: false
thumbnail: /images/empty.jpg
date: 2018-12-17 11:04:15
tags:
- audio
- newbuild
- sketch
categories:
- possys
---

## Kitchen Radio: A Portable Speaker System

As my parents needed a new kitchen-radio as well as a suitable for listening to music when chilling outside around the house, I decided trying to build them an all-in-one solution.

Some requirements where:
 - Internet Radio (due to the poor radio SNR in our region)
 - Bluetooth connectivity
 - Standalone ability (battery needed)
 - loud enough to listen to music outside
 - good sound quality
 - Design should match the one in their kitchen
 - Easy to control (no need for offering a space-control-center like user panel when turning on some music)

Well, a rasperry together with a DAC should do a pretty good job on producing  a fair sound quality.
For Amplifying the sound, I decided to use a logitech sound system, as I already did a project using such a system and was positively suprised by the quality of their components.
Although this is far away from "good sound" it should suit the requirements to a kitchen radio pretty well. I decided to use a Z323 as this one provides 4 Mid-Range Speaker together with a 35 Watts subwoofer. (Shoot on at Ebay for low budget)
Another argument for using a logitech system was, that I knew they are using an internal power supply of 12V which would fit perfectly the requirements for a portable speaker.

For managing radio, bluetooth and all the user-input stuff, Volumio is the system of choice when it comes to easy audio solutions.

Ok, right now, pretty much every checkpoint is fullfilled.

## Let's buy some things...

Right after buying the Z323, I dismounted the speakers as well as the amplifier and power supply:

%TODO: Bild der zerlegten logitech anlage

Here is a Picture of the RPI ZeroW together with a compatible DAC.
As I planned to add anotehr circuit for handling user input, I had to solder two layers for connectors on the DAC Board.

%TODO: Bild des Raspberry+DAC

I decided to invest in an OLED Display as those standard backlight based lcds are not really charming for a kitchen radio.
I will use an "Autoradio"-Poti, which is basically a stereo poti together with an on/off switch and another button for pressing the knob.

%TODO: Bild der Zusatzplatine+Display+Buttons

Now, all these things together, I can start designing the enclosure. As we recently bought and 3D-Printer, I definitely wanted to make use of the possibilities when it comes to Audio-Enclosure design.

***
![Isometric Left](/images/possys/iso.png)

![Isometric Right](/images/possys/isoL.png)

![Isometric Back](/images/possys/isoClosed.png)

![Isometric Back](/images/possys/front.png)

![Isometric Back](/images/possys/back.png)
***

Taking a closer look at the Mid-Range-Speaker enclosure:

***
![MR Speaker](/images/possys/side.png)
***

The red parts are not-printed parts such as speaker, buttons and display...

I have to print every quarter as well as the front- and back-panel separately, due to the printable area. After that I will glue/ screw everything on a 12mm cork plate, which will serve as bottom and top enclosure.
