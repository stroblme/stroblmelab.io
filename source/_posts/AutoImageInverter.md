---
title: Auto Image Inverter
comments: false
thumbnail: /images/aii/icon.png
date: 2019-01-12 21:29:52
tags:
- software
- script
- python
- short-time
- tool
categories:
- aii
---

## AII - Auto Image Inverter: The Idea

First of all: I'm a great fan of dark themed working environment. Especially working late is totally annoying if a bright screen constantly burns your eyes. 
For me there is a simple design rule: 
*If something has information; make it bright. If it doesn't; leave it black.*

However, as I'm currently writing my bachelor thesis and have to accept the fact that black-themed diagrams doesn't suite well on printed paper, I needed to find an easy way to keep the dark theme in my working environment on one side and have white-themed images of diagrams for the thesis on the other side.

Of course, in Microsoft Visio (which I'm using for creating diagrams of all kind), the most simplest way (in fact not that simple) would be the following workflow:
- Select Diagram
- Set Linecolor to black
- Set Textcolor to black
- Export as png
- Set Background as White
- Set Scale on highest (400x400)
- Set Resolution on "From Source"
- Save Image of Diagram
- Revert Linecolor to white
- Revert Textcolor to white

As I'm (oc course) using a default black background on all my visio sheets, I wouldn't see much of the diagram after step 2 and 3... So I would need to revert those colors in step 9 and 10.
The value of the scale is, referring to the Visio docs, the highest one possible. It took some time for me, finding out about that, but was worth as the image resolution was greatly improved afterwards.

You can find my export settings as image below.

***
![Export Settings](/images/aii/exportSettings.png)
***

Nevertheless, there's no need to explain why this workflow is everything but handy.


## The Script

Let's launch the preferred IDE, put it on the screen center, open Firefox, drag it to the right and open a console and drag it to the left: The Python-Development Environment is ready!

Ok here are some basic requirements:
- Easy to launch
- The less user interaction, needed, the better
- Reliable

Sounds like default, so what is the functionality?
- Convert dark-themed images into white-themed ones (invert them)
- Automatically detect images which are already converted, so they won't be inverted again (which would make them black-themed)
- Command-Line User interface to set parameters
- No need to pass screen-sized filepaths; Launch the script in the folder where it should process files
- Automatically detect files in subfolders
- Filtering for selecting specific filetypes

Well quite some stuff, but cutting the story short here: It was done in about 1 hour.

To detect the theme, I simply count the number of black (0) vs. white (255) pixels, after converting the image to grayscale. To speeding this up, I only process every 10th line (configurable from cmd line).

To process cmd line parameters, I use the argparse library. For image processing openCV (what else?) does a good job. Detecting files in subfolders is done by simple iterations and using filepath walks.

***
![Help](/images/aii/help.png)
![Finished Processing](/images/aii/finished.png)
***

The export settings for the visio diagram remain the same as showed previously.

You can find the project on [Gitlab](https://gitlab.com/stroblme/aii)

Of course, there the image processing needs to be improved, and there should be some kind of error handling, but hey, it works quite well without any problems by now. Feel free to contribute!