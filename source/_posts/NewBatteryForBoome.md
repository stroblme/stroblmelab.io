---
title: BooMe - New Battery
comments: false
thumbnail: /images/empty.jpg
date: 2018-11-18 22:18:30
tags:
- rebuild
categories:
- boome
- audio
---

## New Battery Arrived!

As already described in the previous post, I'm about to repair a fished UEBOOM.
I managed to get it playing sound, but only while connected to a charger. As this doesn't really fit the image of a "portable" speaker, I ordered a new LiPo.
You can view the specs [here]()

As the battery is connected directly to a smaller circuit which seems to be responsible for loading and preventing damage due to exhausted.

As this kind of battery only comes with blank plates (Plus/Minus) like the ordinary standard AA-Batteries, it's fairly impossible to solder any wires these plates.

A common method for connecting those kind of batteries is welded.
More precise: Spot-Welded

Luckily, my dad already built such a spot-welder some time ago and therefore the task of reconnecting the protection circuit to the battery didn't took much more than 10 minutes.

From there, the only thing left to do was to reconnect the circuit to the main circuit and mount the battery in it's enclosure.


## Still Not Working

Surprisingly, after I charging the BOOME and trying to turn it on, it didn't worked without power cable being connected.
Before putting in the battery, I measured the voltage, which was something about the expected 3.7V.
Maybe the charging circuit didn't like the water inside..

.. To be continued ..