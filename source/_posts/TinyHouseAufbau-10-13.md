---
title: Tiny House Aufbau - Anstrich und Innendecke
comments: false
thumbnail: /images/tinyhouse/platteTragen.jpg
date: 2019-10-13 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - Anstrich und Innendecke
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-10-13_timelapse.webm" type="video/webm">
    </video>
</div>

Es wurde neben dem äußeren Anstrich, außerdem auch die letzte Dach-Innenplatte eingesetzt, womit dieser Abschnitt ebenfalls abgeschlossen ist:

![Innenplatte](/images/tinyhouse/innenPlattetragen.jpg)
![Innenplatte](/images/tinyhouse/innenPlatteEinsetzen.jpg)

Um die Stoßkanten zu verdecken, wurden, wie auf dem Titelbild zu sehen, Streifen in S-Form gefräst, welche später von unten verklebt werden.

![Innenplatte](/images/tinyhouse/InnenstreifenFräsen.jpg)