---
title: Tiny House - The Beginning
comments: false
thumbnail: /images/empty.jpg
date: 2018-07-08 20:16:55
tags:
- ideas
- stories
categories:
- tinyhouse
---

## Why Do I Want To Live In A Tiny - House ?

### The Idea

My parents may have heard more pleasing statements before when I pointed out that I'm planning to build and live in a Tiny House, somewhen in the mid of this summer.
In Fact I didn't found out about Tiny Houses on myself. A friend of mine told me about that after I tried to convince him,  that living on a Houseboat would be nice living-solution for a student as I'm currently not that far away from the next river and not quit satisfied by living together with two other people in a small appartement somewhere in the town.
Coming from a 300-souls village surrounded by a beautiful forest and enjoying all those resulting benefits for nearly 18 years, I felt myself drawn back into some more natural kind of living after three years of studying in a (for my relatives large) town like Karlsruhe.
So what are the options, given:
- Property-House
- Property-Apartement
- Shared Living
- Some totally different kind of home

Although the first one may be my choice in the long run, it's (of course) not affordable for a student.
The Property-Apartment may be cheaper, is, as well as the house, a long-term investment which binds one to a specific location. As I'm only about 21 it's nearly impossible to predict weather I want to live on the same place for the next 5-8 years.
I've lived in a shared living-apartment for the last three years. There are many advantages especially when it comes to housekeeping-things, but I never experienced that kind of social comfort one may imagine when thinking of a group of students living in the same place.
I can hardly judge my colleges for this, although I can count many and also closer friends, but when it comes to daily routines I'm enjoying those advantages of just carrying for oneself. (Difficult to describe this... :/ )

However, summing up, it comes clear that neither of the first three points are a compromisless choice for me. One can imagine my euphoria when hearing about the idea of living in an own home which may be a little smaller, but offers all those great advantages that a house can bring (distance to neighbors, no responsibility regarding the property-owner, ...) and which is still affordable.

I don't want to bother on describing the idea of a Tiny-House any further as there are a growing number of web-pages also covering this topic.
Furthermore, I hope that I may have inspired someone else to spend a few thoughts on this alternative living idea.
I also want to encourage to read those other posts on my blog. There may be some more inspirations there ;)