---
title: Tiny House - Statik Test Sandwichplatte
comments: false
thumbnail: /images/tinyhouse/StatikTestTitle.png
date: 2019-07-01 08:16:18
tags:
categories:
- tinyhouse
---

# Statik Test der Sandwichplatte

Ein paar Tests um zu überprüfen, wie stabil die Sandwichplatten später sind.
Hier wurde mit 2x12mm Fichten Sperrholz getestet. Verbaut wird stäter Pappel Sperrholz mit 12mm innen und 8mm außen.

Beidesmal wurden die Schichten mittels Ponal Holzleim verbunden.

***

<div class="row post-image-bg">
    <video width="99%" height="540" controls muted>
        <source src="/images/tinyhouse/Messungholz2_LQ.webm" type="video/webm">
    </video>
</div>

***

Anschließend noch ein Zugtest:

***

<div class="row post-image-bg">
    <video width="99%" height="540" controls muted>
        <source src="/images/tinyhouse/BruchTest_LQ.webm" type="video/webm">
    </video>
</div>

***

Es stellte sich bei größerem Werkzeug später heraus, dass anstatt wie erwartet die Leimverbindung, eher die Korkmatte in sich selbst reißt.