---
title: Tiny House Aufbau - Anstrich V2, Dämmung und Fenster
comments: false
thumbnail: /images/tinyhouse/StreichenVorne.jpg
date: 2019-10-14 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - Streichen V2, Dämmung und Fenster
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-10-14_timelapse.webm" type="video/webm">
    </video>
</div>
