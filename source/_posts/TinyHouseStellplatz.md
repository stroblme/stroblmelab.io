---
title: TinyHouse - Stellplatz
comments: false
thumbnail: /images/empty.jpg
date: 2018-11-19 21:22:31
tags:
- ideas
categories:
- tinyhouse
---

***
#### Dieser Blogeintrag zum Thema "TinyHaus" wird ausnahmsweise in deutsch verfasst, da die hier aufgeführten Punkte größtenteils den derzeitig geltenden deutschen Vorschriften bzgl. TinyHaus-Bau geschuldet sind.
***


## Initiative Tiny-Haus Karlsruhe

Durch [Nessa](https://www.youtube.com/channel/UCV7liswUxCOLqV73zUthXow) wurde ich auf eine [Tiny-Haus Initiative in Karlsruhe](https://www.facebook.com/groups/th.initiative.ka/) aufmerksam gemacht.
Sie hat bereits ihr eigenes Tiny-Haus gebaut und auf ihrem Youtube-Kanal über den Bau sowie mittlerweile diverse andere Thematiken geblogt.

Näheres zur Tiny-Haus Karlsruhe Initiative gibts [hier](https://tinyhousepark.de/tiny-house-park-karlsruhe-in-planung/).

Vielleicht die nächste Möglichkeit?


## Campingplatz Ettlingen (Albgau)

Stand 18.11.2018 lassen die Eigentümer des Campingplatzes Albgau keine Stellmöglichkeit für ein Tiny-Haus zu. Nach einem kurzen Telefonat war die Begründung hierzu, dass TH wohl zu hoch für den Campingplatz seien.

... to be continued ...