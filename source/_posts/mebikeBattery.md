---
title: MEbike - Battery
comments: false
thumbnail: /images/mebike/calculations.png
date: 2019-07-22 12:16:08
tags:
- design
- crafted
categories:
- mebike
---

## Finding the Cheapest Battery

The battery is maybe the second most expensive part besides the motor drive.
As I find it nice when the battery doesn't stick out of the bike frame like a huge box, I wanted to craft my own battery pack and adapt it's shape to the bike frame.

Searching for the indivdual cells, I found that, besides the capacity and voltage, there are quite a lot parameters to respect:
- Capacity
- Voltage
- Internal resistance
- Maximum discharge current
- Maximum charge current
- Price

Whilst the price should be as low as possible, the other criteria needs to match at least the criteria of the whole battery pack. That's the point where things are getting tricky.
As you can increase the capacity together with the maximum discharge current by wiring batteries parallel, you can increase your total voltage and together the total internal resistance, by wiring batteries series.
In fact, an increasing internal resistance will cause a drop of the totally allowed discharge current.

In addition to that you have to keep the price in mind, as wiring multiple batteries with higher single voltage in parallel might be cheaper as multiple batteries with higher discharge current in series.

As you might see, calculating all those by hand is nearly impossible. So I wrote an excel sheet doing that job for me.

You can enter the specifications of a cell in a row and the Excel will calculate the amount of cells needed to fit the parameters of the battery pack, specified by the green row. Afterwards, it will sort all cell combinations by price and displays the actual parameters of the resulting battery pack in the orange line.

***
![Table Preview](/images/mebike/calculations.png)
***

You find the table on the corresponding gitlab page [here](https://gitlab.com/stroblme/mebike/blob/master/doc/calculations.xlsm).

If you want to add other cells, please fork the project and make a merge request, or write me an email, so I can include them.
