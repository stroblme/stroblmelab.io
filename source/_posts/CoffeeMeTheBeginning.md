---
title: CoffeeMe - The Beginning
comments: false
thumbnail: /images/coffeeme/depcm01.jpg
date: 2018-08-01 16:23:43
tags:
- design
- rebuild
categories:
- coffeeme
---

## Starting with a Quickmill 5000a

Spending a lot of time, searching for an affordable and also high-quality coffee-machine, I luckily saw an ad on "EbayKleinanzeigen" where a guy sold his "defect" Quickmill 5000a.

I buyed it, took the enclosure and most of the functional components apart, cleaned everything, build it together and.. it worked!
Don't really know what's been the problem before, but hey, now I've got a professional coffee machine for a few bucks!

Here are some pics of how everything looks:

***
![Quickmill5000a](/images/coffeeme/depcm02.jpg)
![Quickmill5000a](/images/coffeeme/depcm03.jpg)
![Quickmill5000a](/images/coffeeme/depcm04.jpg)
![Quickmill5000a](/images/coffeeme/depcm05.jpg)
![Quickmill5000a](/images/coffeeme/depcm06.jpg)
![Quickmill5000a](/images/coffeeme/depcm07.jpg)
***