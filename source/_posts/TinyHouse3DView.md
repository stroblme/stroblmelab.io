---
title: Tiny House - 3D View
comments: false
thumbnail: /images/tinyhouse/tinyHouseV11.jpg
date: 2019-07-10 09:40:26
tags:
- design
categories:
- tinyhouse
---

<!-- Bckg: 1e252d
Ground: 0x333f4c
Dark-Fore: c6ccc1
Fore: f7fff2 -->

{% raw %}

<div class="row post-viewes-bg">
	<div id="3d-viewer" style="width: 100%; height:720px;"></div>
</div>


<script>
	var viewer = document.getElementById('3d-viewer');

	var scene = new THREE.Scene();
	scene.background = new THREE.Color(0x1e252d);
	// scene.fog = new THREE.Fog(0xa0a0a0, 200, 1000);

	var camera = new THREE.PerspectiveCamera(30, viewer.offsetWidth / viewer.offsetHeight, 0.1, 8000);
	camera.position.set(-850, 650, 1250);

	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(viewer.offsetWidth, viewer.offsetHeight);

	var controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
	controls.dampingFactor = 0.1;
	controls.screenSpacePanning = true;
	controls.minDistance = 10;
	controls.maxDistance = 3000;
	controls.maxPolarAngle = Math.PI / 2;
	controls.target = new THREE.Vector3(0,200,0);
	controls.update();

	// ground
	var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(5000, 5000), new THREE.MeshPhongMaterial({
		color: 0x333f4c,
		depthWrite: false
	}));
	mesh.rotation.x = -Math.PI / 2;
	mesh.receiveShadow = false;
	scene.add(mesh);

	// grid
	var grid = new THREE.GridHelper(2000, 20, 0xc6ccc1, 0x1e252d);
	grid.material.opacity = 0.2;
	grid.material.transparent = true;
	scene.add(grid);

	// light
	dirLight_A = new THREE.DirectionalLight(0xffffff, 1);
	dirLight_A.color.setHSL(0.1, 1, 1);
	dirLight_A.position.set(-1, 1.75, 1);
	dirLight_A.position.multiplyScalar(1000);
	dirLight_A.castShadow = true;
	dirLight_A.shadow.mapSize.width = 2048;
	dirLight_A.shadow.mapSize.height = 2048;
	scene.add(dirLight_A);

	dirLight_B = new THREE.DirectionalLight(0xffffff, 1);
	dirLight_B.color.setHSL(0.1, 1, 1);
	dirLight_B.position.set(1, -1.75, -1);
	dirLight_B.position.multiplyScalar(1000);
	dirLight_B.castShadow = true;
	dirLight_B.shadow.mapSize.width = 2048;
	dirLight_B.shadow.mapSize.height = 2048;
	scene.add(dirLight_B);

	dirLight_C = new THREE.DirectionalLight(0xffffff, 0.5);
	dirLight_C.color.setHSL(0.1, 1, 1);
	dirLight_C.position.set(1, 1.75, -1);
	dirLight_C.position.multiplyScalar(1000);
	dirLight_C.castShadow = true;
	dirLight_C.shadow.mapSize.width = 2048;
	dirLight_C.shadow.mapSize.height = 2048;
	scene.add(dirLight_C);


	// Model
	var material = new THREE.MeshPhongMaterial({
		color: 0xf7fff2,
		emissive: 0x111111,
		specular: 0x000000,
		shininess: 0,
		transparent: true,
		opacity: 0.9
	});

	var loader = new THREE.STLLoader();
	loader.load('https://blog.stroblme.tech/models/tinyHouseV11.stl', function(geometry) {
		var mesh = new THREE.Mesh(geometry, material);
		mesh.position.set(0, 0, 0);
		mesh.rotation.set(-Math.PI / 2, 0, 0);
		mesh.scale.set(0.1, 0.1, 0.1);
		mesh.castShadow = true;
		mesh.receiveShadow = true;
		scene.add(mesh);
	});

	// Animate stuff
	var animate = function() {
		requestAnimationFrame(animate);

		controls.update();

		renderer.render(scene, camera);
	};

	animate();

	viewer.appendChild(renderer.domElement);
</script>

{% endraw %}
