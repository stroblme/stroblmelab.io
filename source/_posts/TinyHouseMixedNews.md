---
title: Tiny House - Mixed News
comments: false
thumbnail: /images/empty.jpg
date: 2019-06-25 07:02:50
tags:
categories:
- tinyhouse
---

## Bad and Good News from the Tiny House

#### The bad news first

Referring to the last post ("Tiny House - Final Version") where I reported about the vendor who supplies us with the material for walls and roof, we had some major problems with him regarding the delivery.
Starting whilst building the ground plane, he continued his behaviour of suspending the delivery date from one day to another and from one week to another.
As I wanted to finish the raw building in the end of April, and didn't have any material except the ground plane at the end of Mai, I consulted an lawyer.
The first intention was to build some pressure for getting the material faster, hoping that the vendor wouldn't bring up weak excuses anymore.
The fact, that I want to build this House not at least, because I already invested much money and time in this project lead to a relive high tolerance for any circumstances coming up. Nevertheless, suspending the delivery date so many times, discouraged me a lot and even brought up doubts if I should continue the whole project.
Contacting the lawyer made me more confident, that the project would come to a good end.

#### Following by the good news

Somehow, I begun searching for alternatives on building the house on my own and without the help of the vendor. In contrast to the first approach, I didn't developed a timber stud approach, but searched for materials to craft sandwich plates with cork isolation.
In general, the construction is very similar to the one used by my vendor, but differs in the way that each wall is being build as a whole.
Contrasting to the timber stud construction, the stability doesn't come from vertical wooden planks, but from glueing all three layers (outer wood, isolation, inner wood) together.
So I started searching for the required materials and begun estimating a price. To my surprise, the total sum for the main materials have been nearly 8k cheaper then the original offer from the vendor.
I requested delivery dates for all materials. All could be shipped within one week. .. !!!
Then I calculated the resulting weight, using the selected materials. With 20mm (!!) more isolation and only 2mm thicker walls, I was able to reduce the weight of the whole building by nearly 200kg.
Not hesitating, I contacted the lawyer and told him, that I want to retire from the contract.

#### And the mixed news

As typically for this project, not everything worked as expected.
At first, the vendor retarded the payback. In parallel, the law protection insurance (? Rechtschutzversicherung) questioned the justification for paying the lawyers, as the Tiny House is some kind of building for permanent living, which wasn't covered in the agreement. Luckily, I checked that before contacting the lawyers, and had a written agreement from the insurance, that they would cover the case, so the discussion wasn't that long...

Backed by the great experience regarding delivery dates, I called the vendor for the cork isolation a second time, and, without any surprise, after contacting the manufacturer, he must admit, that the 70mm cork wasn't available that quick. I asked for alternatives, and he offered me 80mm cork, which could be shipped within a few days.
Once again, I calculated the whole weight and optimized the calculation, to only respect actual build area (Before I used same dimensions for inner and outer walls).
I also reduced the thickness of the wood, as the thicker cork would also bring more stability.
In the end, the 10mm more isolation brought 100kg more weight.
Quite acceptable..

So I ordered everything, and believe it or not, everything arrived!

Happy working! ;)
