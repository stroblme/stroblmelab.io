---
title: Tiny House - Holz Transport
comments: false
thumbnail: /images/tinyhouse/HolzbeschaffungTitle.png
date: 2019-07-01 09:11:11
tags:
categories:
- tinyhouse
---

# Abholung des Holzmaterials

Ein Video vom Aufladen des Holzmaterials, über die doch recht außergewöhnliche Fahrt mit dem Tiny House Anhänger, bis zum letztendlichen Einparken am Bestimmungsort.

***

<div class="row post-image-bg">
    <video width="99%" height="540" controls muted>
        <source src="/images/tinyhouse/Holzbeschaffung_LQ.webm" type="video/webm">
    </video>
</div>

***