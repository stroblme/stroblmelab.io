---
title: TinyHouse - Facts
comments: false
thumbnail: /images/empty.jpg
date: 2018-11-13 20:12:50
tags:
- ideas
categories:
- tinyhouse
---

***
#### Dieser Blogeintrag zum Thema "TinyHaus" wird ausnahmsweise in deutsch verfasst, da die hier aufgeführten Punkte größtenteils den derzeitig geltenden deutschen Vorschriften bzgl. TinyHaus-Bau geschuldet sind.
***

## Einschränkungen bzgl. StVZO des TinyHaus

- Höhe: max. 4m
- Breite: max. 2,55m (nicht zu verwechseln mit der für Personenkraftwagen geltenden Breite von 2,50m)
- Masse: 3,5t zulässiges Gesamtgewicht

Nachzulesen auch hier: http://www.verkehrsportal.de/stvzo/stvzo_32.php

## Tipps zur Konstruktion der Wände

- Verstrebung 'X' förmig um Kräfte aus verschiedenen Richtungen abzufangen
- Möglichst einfache Konstruktion (es soll auch in endlicher Zeit anzufertigen sein)
- Wiederverwendung von Maßen um Balken etc. in größeren Mengen ablängen zu können
- Platz für Fenster/ Türen vorsehen

## Zulassung

Es gibt zwei Möglichkeiten ein TinyHouse-On-Wheels zuzulassen.
- Wohnmobil
- Ladung

Ersteres ist die zunächst wohl naheliegenste Möglichkeit. Man muss jedoch beachten, dass hierbei einige Pflichten und Anforderungen zu erfüllen sind:
- Fluchtwege zu jeder Seite (Fenster genügen)
- Abmessungen dürfen nicht minimal überschritten werden
- 

.. Wird noch bei Gelegenheit vervollständigt...