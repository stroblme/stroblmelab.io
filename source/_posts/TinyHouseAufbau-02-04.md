---
title: Tiny House Aufbau - 02-04
comments: false
thumbnail: /images/tinyhouse/tinyHouseTimelapse.jpg
date: 2020-02-04 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - 02-04
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-02-04_timelapse.webm" type="video/webm">
    </video>
</div>
