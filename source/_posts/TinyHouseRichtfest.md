---
title: TinyHouse - Richtfest
comments: false
thumbnail: /images/tinyhouse/richtfest.jpg
date: 2019-12-01 21:46:11
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Vielen Dank an die vielen helfenden Hände!

## Ohne die Unterstützung aus der Familie, sowie des Freundes- und Bekanntenkreises wäre dieses Projekt nun bei weitem nicht möglich gewesen!

---

![Dach](/images/tinyhouse/richtspruch.jpg)
