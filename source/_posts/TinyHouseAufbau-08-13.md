---
title: Tiny House Aufbau - 08-13
comments: false
thumbnail: /images/tinyhouse/tinyHouseTimelapse.jpg
date: 2019-08-13 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - 08-13
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-08-13_timelapse.webm" type="video/webm">
    </video>
</div>
