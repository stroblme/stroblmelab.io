---
title: TinyHouse - HoloTour
comments: false
thumbnail: /images/tinyhouse/v7_holo_title_interior.png
date: 2018-11-25 20:54:57
tags:
categories:
- tinyhouse
---

# Augmented Reality Tour Through My Virtual TinyHouse

Due to my Bachelorthesis, I have access to the Microsoft HoloLens. It's an eyewear device which is, due to it's see-through displays, able to show "Holograms" on surfaces in the real world.

As I already spent a lot of time in planning and designing the TinyHouse, I can't kept my fingers off on trying to load those SketchUp Models into the HoloLens.

***

<div class="row post-image-bg">
    <video width="99%" height="540" controls muted>
        <source src="/images/tinyhouse/TinyHouseHoloTour.mp4" type="video/mp4">
    </video>
</div>

***

How does that look from outside without the Hologram visible?

***

<div class="row post-image-bg">
    <video width="99%" height="540" controls muted>
        <source src="/images/tinyhouse/TinyHouseExt.mp4" type="video/mp4">
    </video>
</div>

***