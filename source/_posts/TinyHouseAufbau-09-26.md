---
title: Tiny House Aufbau - Wände Stellen
comments: false
thumbnail: /images/tinyhouse/aufstellenBckg.jpg
date: 2019-09-26 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Nach vielen Arbeitsstunden...

Zunächst mussten die langen Wände zur Seite gestellt werden.
Begonnen wurde mit der rechten Wand:

![Aufstellen](/images/tinyhouse/aufstellen1.jpg)

Frei schwebend:

![Aufstellen](/images/tinyhouse/aufstellen2.jpg)

Selbes wurde dann für die darunterliegende, linke Seite durchgeführt.

Es folgte die hintere kurze Wand:

![Aufstellen](/images/tinyhouse/aufstellen3.jpg)

Diese wurden nun bereits an die richtige Position auf dem Anhänger gebracht.

![Aufstellen](/images/tinyhouse/aufstellen4.jpg)

Selbiges für die vordere kurze Wand:

![Aufstellen](/images/tinyhouse/aufstellen5.jpg)

Nach dem Abstreben und Sichern der beiden kurzen Wände, wurde nun die linke lange Wand hindurchgeführt:

![Aufstellen](/images/tinyhouse/aufstellen6.jpg)

Und von außen in die entsprechenden Stellen der kurzen Wände gesteckt, sowie verklebt.

![Aufstellen](/images/tinyhouse/aufstellen7.jpg)

Für die rechte lange Wand wurde ähnlich verfahren:

![Aufstellen](/images/tinyhouse/aufstellen8.jpg)

Ein kurzes Video zeigt nochmal die Reihenfolge, in der die Wände aufgestellt wurden:

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/construction.webm" type="video/webm">
    </video>
</div>

Ein Zeitraffer Video des gesamten Abends:

<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-09-26_timelapse.webm" type="video/webm">
    </video>
</div>
