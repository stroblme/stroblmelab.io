---
title: CoffeeMe - Various Improvements
comments: false
thumbnail: /images/empty.jpg
date: 2018-11-17 19:04:53
tags:
- rebuild
- wip
categories:
- coffeeme
---

## Various Improvements made to the Quickmill 5000a during the coffeeMe Project

This post is a summary of different improvements or changes made to the basic construction of the Quickmill5000, not covering the general features added.

***

### Thermally Isolating the Heater from it's Mounting Base

Mounting the heater directly to the metal frame of the coffee-machine doesn't sound like a good idea regarding the heat-up time.
Therefore, I decided to isolate the "Thermoblock" from it's mounting base, by inserting two small silicone pads, "borrowed" from a kitchen util.

![Isolated Thermoblock](/images/coffeeme/thermoblockIso.jpg)

### Re-Placing the Circuit Board

As the circuit board is currently at the place, where I'm planning to put in the water cistern, I mounted the board nearly to the bottom, behind the main frame.

![New Place for Circuit Board](/images/coffeeme/circuitBoard.jpg)

### Adding a Manometer

Luckily, a friend of mine was able to bring up a very cute manometer, which, with some additional pipes, fits perfectly to the whole design.

%TODO
![Manometer]

### Advanced Cup Holder

As design also requires a rethinking of the cup holder, I came up with a pretty useful construction.
In this case, the distance between cup and brewer can be adjusted by moving the cup holder along the threaded rod.

%TODO
![Cupholder]