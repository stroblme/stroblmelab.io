---
title: Git Utils
comments: false
thumbnail: /images/gitUtils/wrench.jpg
date: 2019-06-20 10:00:12
tags:
- software
- utils
categories:
- gitutils
---

## Git Utilities for a faster workflow

When working with git, I noticed that there are some commands which needs to be executed each time you want to make a commit, merge branches, or initialize a repository.
As I always try to automate as much as possible of my daily workflow, I started writing some scripts, extending the Git cmd line interface.

This is a wip project, so features will be added permanently. I don't claim, that the workflow suggested by the use of the scripts, fits for every project and situation. In my case I can work well on most of the projects I solely work on, but also know that for shared projects, those scripts might not represent a "good" workflow.

The following is basically an excerpt of the readme from the corresponding project. You can find the link on the "Projects" page.

## Description

#### git-logg

This will display you a pretty log of the last 40 commits

```
git logg
```

#### git-pushh

This will automatically detect your current branch and push the repo to the remote(s).
In case you specify a remote, it will push only there.
```
git pushh BRANCH
```

In case you have multiple remotes and don't specify one, it will push to all of them but ask you for confirmation for each.
In case you have only one remote, it will push to this single remote without confirmation
```
git pushh
```

#### git-committ

This will call
```
git add ./
git commit -m MESSAGE
git pushh
git logg
```

MESSAGE is what you provide as parameters when calling this command. You don't need to quote your commit message:

```
git committ This is simply your commit message
```

#### git-mergee

This will basically call
```
git fetch . BRANCH_A:BRANCH_B
git checkout BRANCH_B
```

It can automatically detect your current branch, for getting the same result as above, you can simply call:

```
git mergee BRANCH_B
```

Besides you can specify BRANCH_A manually by calling:
```
git mergee BRANCH_A BRANCH_B
```

---

I also added some functionality to register these scripts easily. Just checkout the README
