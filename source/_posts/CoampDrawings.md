---
title: COAMP - Drawings
comments: false
thumbnail: /images/coamp/circuit.png
date: 2018-02-23 20:38:39
tags:
- audio
- newbuild
- sketch
categories:
- coamp
---

## TDA2050 based Compact Operational Amplifier

My first Amplifier-Project using two TDA2050 as main amplifiers together with two TL072 pre-amps.
Aims to fit on a desk, so small size and decent design are prior.
To deliver good stereo sound, which is important due to the near-field setup commonly found on desks, the whole circuit should be designed as two mono-amplifiers using the same power supply.

## Circuit

Below is a screenshot of the circuit I created with KiCad (pretty cool CAD software).

![COAMP circuit](/images/coamp/circuit.png)

I crafted this circuit as a combination from [this](http://www.circuitbasics.com/tda2050-diy-amplifier-build-guide/) article

and some other sources I found on the web (but where I unfortunately cannot find the sources)

The circuit will be soldered on three different PCBs:
- Top Layer Circuit (TL072, 12V PowerSupply, Speaker Protection)
- Mid Layer Circuit (TDA2050, Relais)
- Power Supply (Bridge Rectifier, Capacitors)
