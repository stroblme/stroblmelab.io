---
title: CoffeeMe - Front Panel
comments: false
thumbnail: /images/coffeeme/frontpanel_close.jpg
date: 2018-10-16 13:42:23
tags:
- rebuild
categories:
- coffeeme
---

## Front Panel Deluxe

After some considerations about the front panel design, I found an old terminal-box of an motorblock.
Since it has a nice cable lead-through at the bottom, it was a perfect choice for an outstanding control panel.

As one half of the enclosure was missing, I had to manufacture one, where I can put on all the switches, regulators and led(s).

I choosed some massive-looking switches and decided to merge those 4 status leds into a single rgb led (I will explain the wiring later).

Below you can see the result of this work.

![Full Size Front Panel](/images/coffeeme/frontpanel_full.jpg)
![Closer Look](/images/coffeeme/frontpanel_side.jpg)

This is how the control-panel looked before:

![Deprecated Front Panel](/images/coffeeme/frontpanel_old.jpg)

Regarding the RGB-Led; The default led meaning of the Quickmill was as followed:
- (1) Green: Powered
- (2) Orange: Heating (blinking)
- (3) Red: Error (blinking)
- (4) Orange: No Water

As for me, the "Error" and "No-Water" signal can be combined into a single Led. This results in the following signals:
- Ok: Powered
- Preparing: Heating
- Failure: No Water/ Error

In my case I have an RGB-Led and as blue isn't a preferable status led (There's nothing more awful than a bright blue led burning every cell in your retina), I decided not to use it.
Green is a nice status led for displaying that everything is ok and therefore will be used for continuously signaling that the machine is powered.

- Green: Powered (continuous)
- Orange: Heating (blinking)
- Red: No Water (continuous) / Error (blinking)

As a result the wires (1..4) will be connected as follow:

![Deprecated Front Panel](/images/coffeeme/frontPanelLedCircuit.png)

Due to the low price for little microcontrollers, I will probably use one of the AVRTiny series to realize the logic gatters, as these are only available in multi-gatter packages.