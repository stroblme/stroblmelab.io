---
title: Possys - Assembling
comments: false
thumbnail: /images/empty.jpg
date: 2019-01-06 20:14:15
tags:
- audio
- newbuild
- sketch
categories:
- possys
---

## Time for printing

In the other meaning... It took quite a long time to print all the six parts, but the results are pretty stunning.
After fixing everything with silicon on the cork plate:

%TODO: Bild der gedruckten Teile.

As you can see, the speakers and most other parts fitted perfectly. I only had some problems when printing holes for switches and potentiometer, as something went wrong with the 3D Model which leaded to partly filles holes.

The back enclosure was another tricky part due to all the curves. In first instance, the model wasn't water-proof. Loading up to %TODO Site name
fixed most issues. I discovered that many problems could be resolved by checking for inner planes and lines. After removing those, I became a good result without any 3D model errors.


## Bringing all components together

I decided to go for an internal 230V/ 12V power supply in addition to an optional external 12V power supply.
I reused the original logitech power supply for this purpose as it offered something about 16V which is perfectly fine and added an 5V Step-Down-Converter for powering the RPI Zero. The total power consumption of RPI, Display and DAC shouldn't be that much to make a significant impact on the Logitech power supply (which should be around 40W).

I added another circuit board, with the following tasks:

- Maintaining switching power between external 12V supply and internal 16V
- Providing 5V for RPI and components
- Maintaining switching audio between external AUX input and internal DAC

As it may have huge impact on the input signal, I decided to completely cut off the DAC output via relay when an external audio source is connected.
Same is true for the power supply to provide unnecessary power leakage on the input of the logitech power supply.
