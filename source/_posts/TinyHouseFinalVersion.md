---
title: Tiny House - Final Version
comments: false
thumbnail: /images/tinyhouse/Aufbau_flt.png
date: 2019-02-05 22:17:20
tags:
categories:
- tinyhouse
---

## The Final Version of my Tiny House Sketch

After a lot of effort on finding a suitable solution regarding the framing of my Tiny House, we luckily found a vendor who supplies an extraordinary type of material which perfectly fits the needings of such a building.

![Glued Laminated Timber With Cork](/images/tinyhouse/corkbeam.png)

It's basically two vertical slot-and-key wooden-planks with an isolating cork layer glued in between.
This has some huge advantages over the common timber stud buildings:
- Inner- and outer sheating are combined in a single block
- Enables fast and easy building of walls
- Windows can be cut out after building. No need to worry about that on an early stage
- It's lighter than the timber stud method
- Cork has a similar isolating value to styropor (Ug ~ 0.03-0.04) but is a natural material (Bark of the cork oak)
- Breathing and water resistant layer

Sounds pretty cool, huh? So what are the drawbacks? .. Well, the costs!
If it was worth that.. I can tell you when the project is finished. In the end, I will provide a list of all uses materials and their value.


### Let's sketch it!

Ok, as we are a bit lighter now, I can stick with my block building option (I previously planned to remove the front loft as you can see in the following picture).

![V9 Iso View](/images/tinyhouse/v9_iso.png)

But as this won't provide any possibility of storing things I came op with the following (and hopefully final) version of my TinyHouse Sketch:

***
![V10 Iso View](/images/tinyhouse/Aufbau_flt.png)
![V10 Iso View](/images/tinyhouse/Aufbau_brt.png)
![V10 Iso View](/images/tinyhouse/Aufbau_frt.png)
![V10 Iso View](/images/tinyhouse/Innenausstattung_frt.png)
![V10 Iso View](/images/tinyhouse/Innenausstattung_brt.png)
***

I also added bathroom, with the storage above (back) as well as the kitchen with the bedroom above (front).
