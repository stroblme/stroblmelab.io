---
title: HAUCO - Next Generation
comments: false
thumbnail: /images/empty.jpg
date: 2019-03-04 14:10:59
tags:
- ideas
categories:
- automation
- hauco
---

## Next Generation of Home Automation

During the years of using the previous system, I gathered some ideas on missing features and improvements which could be made to the system. Although, everything was working properly, adding some features would go with mandatory changes on the whole system.

### Requirements

Now, moving to a completely new housing provided a good reason for making those changes:

- Stricter separation of Server, Signal Handler and Driver units
- Direct Access to Signal Handler (adds more redundance)
- Shorting power driven wires
- More flexible design for adding more clients
- Environment sensors

Quite some stuff, huh?

### Design Idea

After a lot of thoughts on this topic, I finally decided to go with the following solution:

- Low-Power standalone computing unit running the OpenHab server (Main Unit)
- FPGA based board for signal processing. Connected via Ethernet to the main unit. Converts the received data into DMX and TWI (Signal Handler)
- DMX Receivers for driving RGBW LED strips and remote relays (Actor Client)
- TWI Slaves for collecting environmental data (Sensor Clients)

With this setup, I was able to fullfil every requirement specified at first.

The DMX and TWI signals will both be transmitted using CAT6 ethernet cables. Those provide 6 twisted-pair shielded wires which therefore should fit the specifications of both protocols.