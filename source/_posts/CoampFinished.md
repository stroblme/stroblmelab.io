---
title: COAMP - Finished
comments: false
thumbnail: /images/coamp/coamp_iso.jpg
date: 2018-03-11 20:40:04
tags:
- audio
- newbuild
- crafted
categories:
- coamp
---

## Stylish Open-Frame Enclosure for COAMP

After I made some drafts on how the PCBs will look like, I begun crafting an enclosure, completely made out of alloy, where the power transformer was the most scaling part.

The transformer is a bit oversized fo the 35 watts of output power, but therefore it stays cool and ensures continuous voltage.

***
![COAMP](/images/coamp/coamp_iso.jpg)
![COAMP](/images/coamp/coamp_front.jpg)
![COAMP](/images/coamp/coamp_side.jpg)
![COAMP](/images/coamp/coamp_bare_iso.jpg)
![COAMP](/images/coamp/coamp_preamp.jpg)
![COAMP](/images/coamp/coamp_preandmainamp.jpg)
![COAMP](/images/coamp/coamp_circuitsclose.jpg)
***