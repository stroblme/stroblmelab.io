---
title: Tiny House - V8
comments: false
thumbnail: /images/tinyhouse/v8_improvement.png
date: 2018-11-02 21:28:56
tags:
- sketch
categories:
- tinyhouse
---

## Roof And Walls Need Updates

During a review of the current sketch (v7) from my roommade, who recently finished his bachelor in architecture, he suggested to redesign the walls, as well as the roof:

- The walls should have a continously X-Shape for stability reasons.
- The roof should be a single component and should have at least slope of 3-4% to ensure that water is drained properly

Both improvements also bring some enhancements on crafting those components, since it always has to be ensured that everything is sealed against water, which is lot more difficult with those three different levels in the roof, as shown in the previous version of the sketch

![Tiny Hosue V8 improvement](/images/tinyhouse/v8_improvement.png)

***

After further improvements, here are some results where I draw some more details regarding every sides as well as floors and the roof:

![TinyHouse V8](/images/tinyhouse/v8_IsoExplode.png)
![TinyHouse V8](/images/tinyhouse/v8_IsoExplode_detail.png)
![TinyHouse V8](/images/tinyhouse/v8_IsoExplode_sheated.png)
