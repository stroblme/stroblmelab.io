---
title: HAUCO - Interface Board
comments: false
thumbnail: /images/hauco/interfaceBoard/view.png
date: 2019-03-09 12:04:59
tags:
- design
- crafted
categories:
- automation
- hauco
---

## Designing the Signal Interface

The interface board is forms the physical layer towards the single sensor nodes and DMX receivers.
It provides two functionalities:
 - Converting the different voltage levels of the DMX signal
 - Galvanic isolation of the DMX interface
 - Converting the SPI data from the FPGA into CAN-Bus (Controller & Transciver)
 - Galvanic isolation of the CAN interface
 - Galvanic isolation of the power supply towards the channel

Those functionalities enable an easy connection of the FPGA board while protecting it from voltage peaks and shortcuts on the network.

***
![Circuit](/images/hauco/interfaceBoard/circuit.png)
![Layout](/images/hauco/interfaceBoard/layout.png)
![3D View on the board](/images/hauco/interfaceBoard/view.png)
***

I decided to use independent protocols for controlling and sensoring to reduce traffic and improve redundance of the system.
Both protocols, CAN and DMX provide great reliability and robustness against emv due to their differential signaling.
Using a SPI-CAN controller reduces the number of required IO pins on the FPGA Board and transfers major part of the logic towards the controller.
The MCP25625 also provides both functionalities, CAN Controller and - Transceiver.

Using and Interface Board also brings a lot of flexibility when it comes to upgrading or renewing the home automation system.