---
title: Tiny House - V7
comments: false
thumbnail: /images/tinyhouse/v7.png
date: 2018-10-09 20:22:39
tags:
- sketch
categories:
- tinyhouse
---

## Furnished And Functional Tiny House

As this is a long-term project started in the mid of July (posts will be there soon), I will only share some updates regarding my work-in-progress SketchUp-Sketch.

I've finished major parts of the Tiny House Sketch and added some furniture to get a better imression how everything can look like. This mght prevent issues when planning the rooms and their dimensions.

***
![TinyHouse V7](/images/tinyhouse/v7.png)
![TinyHouse V7](/images/tinyhouse/v7_2.png)
![TinyHouse V7](/images/tinyhouse/v7_3.png)
![TinyHouse V7](/images/tinyhouse/v7_4.png)
![TinyHouse V7](/images/tinyhouse/v7_top.png)
![TinyHouse V7](/images/tinyhouse/v7_bottom.png)
![TinyHouse V7](/images/tinyhouse/v7_front.png)
![TinyHouse V7](/images/tinyhouse/v7_back.png)
***