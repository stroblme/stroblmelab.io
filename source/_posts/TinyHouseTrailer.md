---
title: Tiny House - Trailer
comments: false
thumbnail: /images/empty.jpg
date: 2019-03-04 12:06:24
tags:
- stories
categories:
- tinyhouse
---

## Der Wahnsinn mit dem Trailer

Ich möchte hier kurz meine Erlebnisse bezüglich der Beschaffung und Zulassung des Anhängers erzählen.

### Beschaffung

Nachdem ich mir etwas Zeit gelassen hatte den Anhänger bei Vlemmix zu ordern, wurde ich Ende Januar von dem Ergebnis des Anrufes bei jenen überrascht, in welchem mir ein Liefertermin Anfang März zugesagt wurde.

Da die Grundplatte bereits Mitte/Ende Februar fertig sein sollte, waren das natürlich keine guten Nachrichten.
Durch Zufall bin ich kurz darauf auf ein Ebay-Kleinanzeigen Inserat aufmerksam geworden, in welchem ein gleiches Modell zum Sofortkauf angeboten wurde.
Nach einem kurzen Anruf stellte sich heraus, dass es sich bei dem Anbieter (Actrass) wohl um einen Vetreiber von Vlemmix Anhänger handelt. Mir wurde bestätigt, dass ein Anhänger bereits Mitte Februar direkt vor die Haustür für ca. 4k€ (600€ günstiger als der Preis lt. Vlemmix) geliefert werden könne.

Als der Anhänger nun (mit etwas Verzögerung) ankam, war die nächste Aufgabe nun diesen Zuzulassen.


### Zulassung

Der Lieferant des Anhängers hatte abgesehen von COC-Papieren und Rechnung keine weiteren Dokumente mitgegeben, jedoch habe ich zunächst auf seine Aussage "... für die Zulassung genügen die COC-Papiere und die Rechnung..." vertraut und bin am darauffolgenden Tag motiviert zu der Zulassungsstelle nach Sinsheim gefahren.

Das ganze war dann auch recht schnell erledigt, denn dort wurde mir mitgeteilt, dass entgegen der Aussage des Lieferanten eine Zulassung ausschließlich mittels COC-Papieren nicht möglich sei. Man müsse, da kein Fahrzeugbrief vorliege, die Fahrzeugidentifikationsnummer des Anhängers mit der in den COC-Papieren abgleichen.

Nun muss man an dieser Stelle wissen, dass die Zulassungsbehörde in der Innenstadt liegt, und allein die Vorstellung dort einen 10m langen Anhänger vorzufahren nicht gerade ermutigend ist.

![Zulassungsstelle Sinsheim](/images/tinyhouse/sinsheimLocation.png)

Davon abgesehen, dass ich zuvor ohne Anhänger nach Sinsheim fahren müsste, dort einen Vorführschein abholen und anschließend mitsamt dem Anhänger erneut nach Sinsheim fahren müsste.

Wieder daheim bot ein örtlicher TÜV-Beamte an, ein Schreiben aufzusetzen, in welchem er bestätigt, dass die Fahrzeugnummer auf dem Anhänger, mit der in den COC-Papieren übereinstimmt.
Gesagt, getan bin ich bald darauf erneut nach Sinsheim gefahren um dort nun endlich den Anhänger zuzulassen. Diesmal hat es etwas länger gedauert bis ich draußen war, da die Aussage der Angestellten dort, warum sie den Bescheid des TÜV-Beamten nicht anerkennen bei mir auf pure fassungslosigkeit gestoßen war.

Diese hatte mir zuvor versichert, dass die Bescheinigung zusammen mit den COC-Papieren ausreichen würde um den Anhänger zuzulassen. Wie sich herausstellte, ist dies in der benachbarten Zulassungsstelle in Mosbach auch der Fall. In Sinsheim allerdings nicht.

Um das ganze auf die Spitze zu treiben habe ich kurzerhand in Mosbach angerufen und mich dort erkundigt. Die Aussage dort war:
"Sie könnten den Ahänger bei uns zulassen, ohne diesen Vorführen zu müssen. Es genügt die Bescheinigung vom TÜV. Wenn Sie den Anhänger allerdings in Sinsheim zulassen, müssen Sie diesen bei uns vorführen. Dann übermitteln wir Sinsheim die Bestätigung. Die Bescheinigung des TÜVs genügt dann aber nicht."
...
???
...

Nach einem kurzen Telefonat mit dem bereits erwähnten TÜV-Beamten (welcher umgehend Rücksprache mit der Zulassungsstelle in Sinsheim gehalten hat, da ihm das ganze wohl auch etwas komisch vorkam), stellte sich heraus, dass eine weitere Möglichkeit darin bestand, eine Vollabnahme des Anhängers zu machen (HU). Diese würde dann das Vorführen ersetzen.

Aus Fehlern gelernt rief ich diesmal nun bei der Zulassungsstelle an und lies mir diese Aussage telefonisch bestätigen.. Es stimmte.

Will heißen:
- Bescheinigung des TÜV, dass Fahrzugnummer korrekt ist: Wird nicht akzeptiert
- Bescheinigung des TÜV, dass Fahrzeug den Anforderungen einer HU entspricht: Wird akzeptiert. Fahrzeugnummer ist dann egal.

...
???
...

![BRD in ganzer Pracht](/images/tinyhouse/bananenrepublik.jpg)