---
title: CoffeeMe - Rack
comments: false
thumbnail: /images/coffeeme/cmGestell_mounted.jpg
date: 2018-11-07 16:23:43
tags:
- design
- rebuild
- crafted
categories:
- coffeeme
---

## One-point-mounted, free hanging coffee-machine

I'm excited to share some news regarding the coffeeMe project.
Since an ordinary ground-plate, where everything is mounted on top, wouldn't be that exceptional, I decided to mount everything to a rack which itselfs stands upon four stilts.

***
![Isometric View](/images/coffeeme/cmGestell_iso.png)
![Top View](/images/coffeeme/cmGestell_top.png)
***

Now, there is the result:

***
![Bottom View](/images/coffeeme/cmGestell_actual_bottom.jpg)
![Top View](/images/coffeeme/cmGestell_actual_top.jpg)
***

Build everything together:

***
![Mounted Rack](/images/coffeeme/cmGestell_mounted.jpg)
***
