---
title: BOOME
comments: false
thumbnail: /images/boome/boome_h.jpg
date: 2018-10-21 16:23:13
tags:
- audio
- design
- rebuild
categories:
- boome
- audio
---

## A beautiful result of a fished, repaired and redesigned UEBOOM2

In the end of October'18 I was sitting at a small lake, when I spotted a UEBOOM2 sticking out of the mud at the bank.
I asked some nearby guy, who maintained a little boat renting, for some fishing tool and dragged out the mouldered-looking small speaker.

I didn't expect it would work anymore, but aimed to prevent the leakage of the battery inside, which would have ruined the lake.
After I fished it out (it would have been in this lake for at least a few months, since it was totally covered by algea), I went home and cleaned the speaker.

There was already water inside (the seal was not properly closed, and hey it's only IP69), so I didn't care about that when I washed it out with clean water to remove all the mud. That was how it looked:

![Cleaned UEBOOM2](/images/boome/cleanedboom.jpg)

I removed the coverage and the side elements which include usb charging port, audio input as well as on/off and bluetooth pairing button. Luckily the battery was not damaged (but pretty low), so I was able to remove it easily.

![Unscrewed BOOOM](/images/boome/unscrewedboom.jpg)

After some further work, including the cleansing of all oxidativ contacts, I was able to make a basic setup using only the main circuit, together with the two speakers and the battery as a dummy to trick the charging circuit.

![Bare BOOM](/images/boome/bareboom.jpg)

I cleaned the circuit from any remaining dirt and removed results of electrolytically processes between any contacts.
Keeping the USB charger plugged in, I pressed the ON/OFF Button and....

### It worked!!!

Before, I tried to power the speaker via my desktop-powersupply, which surprisingly caused an "low-battery" warning (?!).

However, after another day of redesign I had the result, which you can see in the header of this post as well as below:

![Full View of BOOME](/images/boome/boome_full.jpg)
![Full View of BOOME](/images/boome/boome_side.jpg)

I really like the finish of the buttons, which is simply a drip of two-component-glue together with two different-sized shims.