---
title: Tiny House Aufbau - 10-07
comments: false
thumbnail: /images/tinyhouse/tinyHouseTimelapse.jpg
date: 2019-10-07 00:00:00
tags:
- timelapse
- blog
- crafted
categories:
- tinyhouse
---

# Tiny House Aufbau - 10-07
## A Timelapse


<div class="row post-image-bg">
    <video width="99%" height="720"  controls muted loop>
        <source src="/images/tinyhouse/TinyHouseAufbau-10-07_timelapse.webm" type="video/webm">
    </video>
</div>
