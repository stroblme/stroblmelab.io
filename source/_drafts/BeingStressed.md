---
title: Being Stressed
comments: false
thumbnail: /images/empty.jpg
date: 2019-10-02 20:24:54
tags:
- stories
- blog
categories:
- general
---

# .. in a good way?

In general, saying that a person is stressed is mostly associated with a negative attitude as the causes of stress are often caused by heavy workload, bad time-management or social circumstances.

If I'm feeling stressed, in most cases I rooted the causes down to lot of work. Either issued by exams or deadlines from the university or project appointments at work.

In the past half year most of stress came from my Tiny House project, especially the related organizational stuff, as well as the exam period at the university.

After my last exam I felt a bit more relaxed and when the walls of the Tiny House finally shaped a building last week, there wasn't any reason to feel stressed anymore.
I had no pending exams where I would need to study for, neither project appointments at work, nor the permanent concerns if all parts of the Tiny House would fit together.

I felt relaxed. No need to be harsh to anyone or getting nervous of dinner takes longer as expected.
Somehow, this relaxed feeling didn't held on that long. I started picking up paused projects or tasks which were not very important to me before.

After a while I begun stressing myself on finishing those tasks and noticed that I tried to priorize those projects above other daily life ..