---
title: TinyHouseBodenplatte
comments: false
thumbnail: /images/empty.jpg
date: 2019-03-17 13:52:39
tags:
- wip
categories:
- tinyhouse
---

## Aufbau der Bodenplatte des Tiny House

Nach längerem hat nun endlich der Bau der Bodenplatte begonnen. Zuerst wurde eine 10x50 Anti-Rutschmatte auf die Rahmenkonstruktion des Anhängers aufgebracht.
Diese woll kleine Rutschbewegungen des Grundplatte verhindern. Die Grundplatte selbst wird über vier "Twistlocks", wie sie auch bei Containern verwendet werden,  am Trailer befestigt.

%TODO Bild Antirutschmatten

Die Twistlocks wiederum sind auf abgewinkelten Stahlplatten angeschraubt, welche ihrerseits über Bügelschrauben mit dem Rahmen des Anhängers verbunden werden.
Dies ermöglicht es theoretisch den gesamten Aufbau von dem Anhänger zu trennen, was aus Gründen der Zulassung des TH als "Ladung" sinnvoll ist.
Inwieweit ich dies später tatsächlich praktizieren werde sei mal dahingestellt.

Im Folgenden sind Bilder der Schweißkonstruktion ohne "Innenteil" während dem behandeln mit sog. "Zinkspray" um eine Korrosion des Materials zu verhindern.

***
![Sprayed Twistlocks](/images/tinyhouse/twistlocksSprayedBottom.jpg)
![Sprayed Twistlocks](/images/mufde/twistlocksSprayedTop.jpg)
***

So sieht das ganze dann montiert am Trailer aus:

%TODO Bild montierter Twistlock

Im nächsten Schritt wird die Rahmenkonstruktion auf dem Trailer aufgebaut. Die MFD Platten werden anschließend zugeschnitten und auf die Rahmenkonstruktion angepasst. Hierbei ist es wichtig an den Stoßkanten größere Spalte zu vermeiden und ggf Unebenheiten Rahmenkonstruktion abzuschleifen.

Da es passieren kann, dass die Rahmenkonstruktion, bedingt durch den Aufbau mit Holz, etwas verzogen ist, sollten die MFD Platten zunächst nur in Bereichen in denen dies nicht der Fall ist befestigt werden. Ist dies geschehen, kann durch Klopfen und Spannen, die Rahmenkonstruktion auf dem Trailer verzogen werden. Durch das Montieren der MFD Platten lässt sich dieser Verzug dann fixieren.

%TODO Bild montierete Bodenplatte mit MFD

Im nächsten Schritt müssen die MFD Platten nacheinander wieder abgeschraubt und die Rahmenkonstruktion an entsprechenden Stellen mit Silikon bestrichen werden.
Der Stoß der MFD Platten wird hierbei ebenfalls mit Silikon abgedichtet.

Ist dies geschehen, wird die gesamte Konstruktion gewendet.

Nun können die Aufnahmen für die Twistlocks angebracht werden.

Es folgt nun die Einbringung der Dämmung. Hierbei habe ich mich, zu Gunsten der Trittfestigkeit, für Styropor entschieden.

%TODO Bild wenn styropor drin ist

