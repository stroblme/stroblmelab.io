---
title: Mufde The Beginning
comments: false
thumbnail: /images/mufde/seated.png
date: 2019-02-11 21:18:28
tags:
- crafted
categories:
- mufde
---

---
#### Patent pending!
---

## This is the Beginning of the MultiFunctional Desk Project

In view of my upcoming Tiny House project, I'm planning to build a new working-desk with focus on the limited space I'll later have.
Some requirements I have for a new desk are showed below. Those where mainly statued from experiences of my desks at work and those for studying.

- Standing - Desk possibility
- Integration of all IT-components (laptop, monitor, mouse, keyboard, usb hub,...)
- Integration with the Home Control environment
- Clean design
- Friendly on space
- Possibility to watch film when friends are around

Lot of things, huh?

#### Sketch

I begun sketching my idea in SketchUp and quickly moved to Autodesk Inventor out of the need for more detailled design constraints.
In Sketch Up, I also put everything in a neat environment to demonstrate the different use-cases.

***
![Overview](/images/mufde/overview.png)
![Seated View](/images/mufde/seated.png)
![Standing View](/images/mufde/standing.png)
![Standing View with Angle](/images/mufde/standingAngle.png)
![Leaving View](/images/mufde/leaving.png)
***

Those pictures should be self-explanatory when thinking about it. The main concept idea is, that the desk plate can folds upwards while providing the possibility to switch between seating and standing usage. Furthermore, the whole desk plate can be adjusted in any angle to fit the users preferences

##### Initial Idea

The plate is moved by two threaded rods mounted vertically. To make the folding mechanism work without any additional motors, the lower few cm of the threaded rod are plank, so the lower mount will "wait" there while the upper one comes closer. Meanwhile, changing the distance between the two mounts will force the desk plate to fold upwards. When "unfolding" the desk, the upper mount will go to a specified distance and then will "pick up" the lower mount on the threaded rod by a steel wire. When the two mounts move with a constant distance, the desk plate will follow this movement with a stable upward/ downward movement.

##### Extended Idea

As the folding mechanism described in the section above is fragile on the picking up part, I preferred to go with the idea of inserting a second threaded rod. Now there is a separate threaded rod for both brackets, which then can be moved individually.

Luckily, Inventor provides the possibility to simulate constraints and component movements. So I rendered a short animation to show the main functionality of the desktop:

<div class="row post-image-bg">
    <video width="99%" height="540" autoplay loop muted>
        <source src="/images/mufde/tableBase.mp4" type="video/mp4">
    </video>
</div>

##### Mounting

The desk is designed to be wall-mounted which is helpful for keeping the floor area clean.
