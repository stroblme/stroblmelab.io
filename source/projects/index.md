---
title:
layout: page
comments: false
thumbnail: /images/projects/projects_h.png
---


This is a listing of some major projects I'm currently working on, or which are already finished.
They are sorted by date started. In the next time I will create a gitlab repo, if not already existing, and link it here.


# Lists and Gitlab links

### CoffeeMe

This project is dedicated to the lovely smell of fresh-grounded, hot-brewed coffee.
Soon, there will be a Gitlab repo where I will upload the schematics.

### HAUCO

https://gitlab.com/stroblme/hauco

### TinyHouse

https://gitlab.com/stroblme/tinyhouse

### MUFDE (coming soon)

https://gitlab.com/stroblme/mufde

### MEBike

https://gitlab.com/stroblme/mebike

### IMMAS

https://gitlab.com/stroblme/immas

### NatLight

https://gitlab.com/stroblme/naturalLight

### Priceler

https://gitlab.com/stroblme/priceler

### COAMP

https://gitlab.com/stroblme/coamp

### POSSYS

https://gitlab.com/stroblme/possys

### AII

https://gitlab.com/stroblme/aii

### GitUitls

https://gitlab.com/stroblme/gitutils


# General Ideas

### Gitutils

Would be nice to add a functionality to automatically detect submodules and create a .gitmodules from that information

### NatLight

Implement deamon functionality. Access information via api