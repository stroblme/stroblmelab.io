[![pipeline status](https://gitlab.com/stroblme/stroblme.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/stroblme/stroblme.gitlab.io/commits/master)

This is the repo of my personal gitlab page at

https://stroblme.gitlab.io

Just a collection of various project descriptions, stories and ideas

---

This repository uses GIT LFS for video files. Please consider that when cloning:

´´´
git lfs install --skip-smudge
git clone https://gitlab.com/stroblme/stroblme.gitlab.io.git
´´´
