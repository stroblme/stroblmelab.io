<img src = "http://onh0umlhz.bkt.clouddn.com/githubhexothemereadmemain.png" width "=" 100% "/>

## Inhalt Inhalt

- Allgemeines
- Demo-Demo
- Schnellstart Schnellstart
- Dokumentation von Dokumenten
- Beitragsbeitrag
- Lizenz Lizenz

## Allgemeines

- Theme-Entwicklung Wenden Sie eine große Anzahl von [HTML5 UP] [2] -Vorlagen an, da der Autor das vordere weiße CSS ist
Ich danke Ihnen allen für Ihre Unterstützung.

## Demo Demo

Persönliche Website: [miccall.tech] (http://miccall.tech)

Liste der Themenartikel:
<img src = "http://onh0umlhz.bkt.clouddn.com/githubhexothemereadmesecond.png" width "=" 100% "/>
Themenartikel Startseite:
<img src = "http://onh0umlhz.bkt.clouddn.com/githubhexothemereadmepost.png" width = "100%" />
Theme Gallery Seite:
<img src = "http://onh0umlhz.bkt.clouddn.com/githubhexothemereadmegallery.png" width = "100%" />

## Schnellstart Schnellstart

- Bitte lesen Sie die offizielle Hexo-Dokumentation, um die [Installation] von Hexo abzuschließen (https://hexo.io/zh-cn/docs/index.html#%E5%AE%89%E8%A3%85) und [ Grundkonfiguration] (https://hexo.io/zh-cn/docs/configuration.html).

- Laden Sie dieses Design herunter und legen Sie es im Verzeichnis "themes" ab, ändern Sie den Namen des Themas und aktivieren Sie es in der Konfigurationsdatei "_config.yml" im Stammverzeichnis der Site.

`` `
    # Erweiterungen
    ## Plugins: https://hexo.io/plugins/
    ## Themes: https://hexo.io/themes/
    Thema: Miccall

`` `
- Ändern Sie den entsprechenden Wert in der Konfigurationsdatei des Themas `_config.yml`.

## Docs Dokumentation

Nachfolgend sind die Themenkonfigurationsdateien `_config.yml` aufgeführt (unterscheiden Sie sie von der Konfigurationsdatei der Site).

##### 一. Kopfetikett

1. Logo der Website
    ** favicon: ** "/img/logo_miccall.png"

2. Durchsuchte Keywords:
Die durch Schlüsselwörter bereitgestellten Schlüsselwörter werden normalerweise von Suchmaschinen zum Klassifizieren von Webseiten verwendet.
Für eine Webseite können mehrere Schlüsselwörter bereitgestellt werden. Mehrere Schlüsselwörter sollten durch Leerzeichen getrennt werden.
Definieren Sie nicht zu viele Schlüsselwörter für die Seite, es ist am besten, sie unter 10 zu halten, zu viele Schlüsselwörter. Die Suchmaschine ignoriert sie.
Definieren Sie keine Schlüsselwörter für Webseiten, die sich nicht auf den Inhalt der Webseitenbeschreibung beziehen.
Da Webseitenersteller Schlüsselwörter missbrauchen (zu viele Schlüsselwörter oder Schlüsselwörter, die sich nicht auf Webseiten beziehen), reduzieren die derzeit verwendeten Suchmaschinen die Wichtigkeit von Schlüsselwörtern.
    ** Schlüsselwörter: ** Miccall

3. Hintergrundbild der Homepage: Der Standardlink lautet "img / bg.jpg" im folgenden Verzeichnis der Hauptstation, dh "miccall.tech/img/bg.jpg".
    ** Hintergrundbild: ** "img / bg.jpg"


##### Int. Intro: Die Schnittstelle, die das Design gerade mit dem Laden begonnen hat.
    
Name
    ** name: ** MICCALL

2. Zweiter Slogan
    ** Motto: ** "Was immer es wert ist, es zu tun, ist es wert, es gut zu machen. Ich werde an jeden Schritt des Weges denken."

3. Der Text über der Home-Taste
    ** HeadButton: ** MICCALL

##### 三 .Nav: Navigationsleiste

`` `
    Heimatname: Name der Heimatnummer
    Is_use_categories: true # Gibt an, ob die Klassifizierung aktiviert werden soll
    Kategoriename: Kategorie # Kategoriename
    Is_use_archives: false # Gibt an, ob die Archivierung aktiviert werden soll
    Archivname: Archiv # Archivname
    Symbol: # Symbol in der Navigationsleiste
        Github:
            Verwendung: true # ob aktiviert werden soll
            Link: https://github.com/miccall # Klicken Sie auf die Adresse
        Twitter:
            Verwenden Sie: falsch
            Link:
        Facebook:
            Verwenden Sie: falsch
            Link:
        Instagram:
            Verwenden Sie: falsch
            Link:
    Seiten:
    # 定制 连接 页
    Der Parameter von # link ist ein relativer Pfad, der dem entsprechenden Ordner im Quellordner unter dem Hexo-Verzeichnis entspricht.
        Wiederaufnahme:
            Link: "/ über /"
        Team:
            Link: "/ group /"
        Galerie:
            Link: "/ gallery /"
        Label:
            Link: "/ tag /"
        #Custom Tag-Name
        # link: "Pfad"

    MainFirst: # Homepage unterhalb der Navigationsleiste
        Name: Miccall Metro # 大头 名 名
        Beschreibung: Willkommen in meinem Blog # 第二 标签 Beschreibung
        Pic_url: /img/me.jpg # Bildadresse
        Goto_ulr: "" # Klicken Sie, um zu springen

    Galerie: # Galerie Seite
        Titel: Mr.metro
        Beschreibung: Nur eine weitere gute Antwort
`` `

Wie zu verwenden:

1. Erstellen Sie eine "About" -Seite

    - Erstellen Sie im `source`-Verzeichnis des Stammverzeichnisses der Site ein neues` about'-Verzeichnis, das `link:" / about / "` entspricht, und erstellen Sie eine ʻindex.md`-Datei, die `about` erstellt `Inhalt.

1. Erstellen Sie eine "Gruppenseite"

    - Erstellen Sie im `source`-Verzeichnis des Stammverzeichnisses der Site ein neues` group`-Verzeichnis, das `link:" / group / "` entspricht, und erstellen Sie eine `index.md'-Datei.
    - Fügen Sie Eigenschaften zu "front-matter" hinzu

        `` `
        ---
        Titel: Gruppe
        Datum: 2017-01-17 21:05:04
        Layout: Links
        ---
        `` `
    - Dann im `source`-Verzeichnis` _data`-Verzeichnis des Stammverzeichnisses der Site (neues Verzeichnis, falls nicht). Erstellen Sie dann eine neue `links.yml`-Datei und fügen Sie nacheinander mehrere Mitglieder hinzu.
        `` `
        # 会员 1
        Name:
              Link: Klicken Sie auf die Verbindungsadresse
              Avatar: Avatar-Adresse
              Descr: Beschreibung
        # 会员 2
        Name:
              Link: Klicken Sie auf die Verbindungsadresse
              Avatar: Avatar-Adresse
              Descr: Beschreibung
        `` `

1. Erstellen Sie eine "Galerie" -Seite

    - Erstellen Sie im `source`-Verzeichnis des Stammverzeichnisses der Site ein neues` galler`y-Verzeichnis, das `link:" / gallery / "` entspricht, und erstellen Sie eine `index.md'-Datei,` front-matter ' `Fügen Sie Eigenschaften hinzu

        `` `
        ---
        Titel: Galerie
        Datum: 2017-01-17 21:39:03
        Layout: Galerie
        ---
        `` `

    - Dann im `source`-Verzeichnis des Site-Root-Verzeichnisses` _data`-Verzeichnis (neues Verzeichnis falls nicht). Erstellen Sie dann eine neue `gallery.yml`-Datei, fügen Sie nacheinander mehrere Bilder hinzu und wiederholen Sie den Bildnamen nicht.

        `` `
        Bildname 1:
          Full_link: Bildadresse
          Thumb_link: Thumbnail-Adresse
          Descr: Bildbeschreibung
        Bildname 2:
          Full_link: Bildadresse
          Thumb_link: Thumbnail-Adresse
          Descr: Bildbeschreibung
        `` `

1. Erstellen Sie eine "Tag" -Seite


    - Legen Sie im `source`-Verzeichnis des Stammverzeichnisses der Site ein neues` tag`-Verzeichnis an, das `link:" / tag / "` entspricht, und erstellen Sie eine `index.md'-Datei,` front-matter '. Fügen Sie Eigenschaften hinzu.

        `` `
        ---
        Titel: Tags
        Datum: 2017-01-17 21:39:14
        Layout: Tags
        ---
        `` `
1. Ändern Sie das Hintergrundbild: Ersetzen Sie `bg.jpg` in` \ themes \ miccall \ source \ images`, ändern Sie den Namen nicht, andernfalls müssen Sie den CSS-Inhalt ändern.

1. Ändern Sie das persönliche Bild unterhalb der Navigationsleiste: Ersetzen Sie "me.jpg" in "\ themes \ miccall \ source \ img", ändern Sie den Namen nicht, andernfalls müssen Sie den CSS-Inhalt ändern.

1. Die Schriftartdatei wird nicht zur Änderung empfohlen.

1. Kommentarsystem: `use` Konfigurationsoptionen` false | duoshuo | disqus | disqus_click | changyan`

    `` `
    Kommentar:
       Verwenden Sie: disqus_click
       Kurzname: http-miccall-tech
       Duoshuo_thread_key_type: Pfad
       Duoshuo_embed_js_url: "https://static.duoshuo.com/embed.js"
       Changyan_appid:
       Changyan_conf:
       Changyan_thread_key_type: Pfad
    `` `

1. Suchsystem:

    `` `
    Suche:
      Verwenden Sie: Google
      Swiftype_key: Nur eine weitere gute Antwort
    `` `

    Verkehrssystem

    Leancloud Views
    Leancloud:
        Aktivieren Sie: false
        App_id: # Ihre App_id
        App_key: # Ihr App_key
        Av_core_mini: "https://cdn1.lncld.net/static/js/av-core-mini-0.6.1.js"

    Busuanzi:
      Aktivieren: true # ob aktiviert werden soll
      All_site_uv: true # globale Aktivierung
      Post_pv: true # separater Artikel aktiviert
      Busuanzi_pure_mini_js: "https://dn-lbstatics.qbox.me/busuanzi/2.3/busuanzi.pure.mini.js"

    Beginnen Sie mit der Erstellung Ihres Artikels: Das Artikelverzeichnis erstellt eine neue .md-Datei im Verzeichnis blog / source / _post:

    ---
    Titel: #Artikel-Titel
    Datum: 2017/3/27 13:48:25 # Veröffentlichungszeit des Artikels
    Tags:
    - Tag 1
    - Tag 2 (optional)
    Kategorien: Algorithmus # Kategorien
    Miniaturbild: https: //xxxxxxxxxx.png # Miniaturbild
    ---

    Artikel Körper